import 'dart:convert';

class Activity {
  final int type;
  final int id;
  final int lessonid;
  final String title;
  final String additionalfield;
  final String typename;

  Activity(
      {this.typename,
      this.type,
      this.id,
      this.lessonid,
      this.title,
      this.additionalfield});

  factory Activity.fromJson(Map<String, dynamic> json) {
    return Activity(
        type: json["type"],
        id: json['id'],
        lessonid: json['lesson_id'],
        title: json['title'],
        additionalfield: json['additional_field'],
        typename: json['type_name']);
  }

  static List<Activity> activitiesFromJson(dynamic json) {
    var activities = json['activities'];
    if (activities != null) {
      var results = List<Activity>();
      print(results);
      activities.forEach((v) {
        results.add(Activity.fromJson(v));
      });
      return results;
    }
    return List<Activity>();
  }
}

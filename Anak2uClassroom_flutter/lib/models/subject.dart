class Subject {
  final String name;
  final String iconUrl;
  final int id;
  Subject({this.name, this.iconUrl, this.id});

  factory Subject.fromJson(Map<String, dynamic> json) {
    return Subject(
        name: json['subject']['name'],
        iconUrl: json['subject']['icon_url'],
        id: json['subject_id']);
  }
  static List<Subject> subjectsFromJson(dynamic json) {
    var subjects = json;
    if (subjects != null) {
      var results = List<Subject>();
      subjects.forEach((v) {
        results.add(Subject.fromJson(v));
      });
      return results;
    }
    return List<Subject>();
  }
}

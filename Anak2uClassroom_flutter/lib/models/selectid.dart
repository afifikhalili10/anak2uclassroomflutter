class Student {
  final String name;

  final int id;
  Student({this.name, this.id});

  factory Student.fromJson(Map<String, dynamic> json) {
    return Student(
      name: json['student_name'],
      id: json['student_id'],
    );
  }
  static List<Student> studentsFromJson(dynamic json) {
    var students = json;
    if (students != null) {
      var results = List<Student>();
      students.forEach((v) {
        results.add(Student.fromJson(v));
      });
      return results;
    }
    return List<Student>();
  }
}

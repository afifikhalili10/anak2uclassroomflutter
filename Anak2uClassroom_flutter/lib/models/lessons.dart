class Lesson {
  final int id;
  final String title;
  final String iconUrl;
  final int lessonid;
  final String additionalfield;
  final String typename;
  Lesson(
      {this.id,
      this.title,
      this.additionalfield,
      this.iconUrl,
      this.lessonid,
      this.typename});

  factory Lesson.fromJson(Map<String, dynamic> json) {
    return Lesson(
        id: json['id'],
        title: json['title'],
        iconUrl: json[""],
        lessonid: json['lesson_id'],
        additionalfield: json['additional_field'],
        typename: json['type_name']);
  }

  static List<Lesson> lessonsFromJson(dynamic json) {
    var lessons = json;
    if (lessons != null) {
      var results = List<Lesson>();
      print(results);
      lessons.forEach((v) {
        results.add(Lesson.fromJson(v));
      });
      return results;
    }
    return List<Lesson>();
  }
}

class Quizzes {
  final int qtype;
  final int id;
  final int quizid;
  final String question;
  final List options;

  Quizzes({this.qtype, this.id, this.quizid, this.question, this.options});

  factory Quizzes.fromJson(Map<String, dynamic> json) {
    return Quizzes(
        qtype: json["qtype"],
        id: json['id'],
        quizid: json['quiz_id'],
        question: json['question'],
        options: json['options']);
  }

  static List<Quizzes> questionsFromJson(dynamic json) {
    print(json);
    var questions = json['questions'];
    if (questions != null) {
      var results = List<Quizzes>();

      questions.forEach((v) {
        results.add(Quizzes.fromJson(v));
      });
      print(results);
      return results;
    }
    return List<Quizzes>();
  }
}

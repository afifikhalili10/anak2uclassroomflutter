import 'package:flutter/material.dart';
import 'package:flutter_auth/Screens/Login/components/body.dart';
import 'package:flutter_auth/models/Quizzes.dart';
import 'package:get/get.dart';
import 'package:flutter_auth/constants.dart';
import 'package:flutter_auth/controllers/question_controller.dart';
// ignore: unused_import
import 'package:flutter_auth/models/Questions.dart';

import 'question_card.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class BodyQuiz extends StatefulWidget {
  final int quizid;
  BodyQuiz({this.quizid});

  @override
  _BodyQuizState createState() => _BodyQuizState();
}

class _BodyQuizState extends State<BodyQuiz> {
  var questions = [];
  var currentquestions = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getquestions(widget.quizid).then((value) {
      print("questions");
      print(value);
      setState(() {
        questions = value;
      });
    }).catchError((error) => {print(error)});
  }

  @override
  Widget build(BuildContext context) {
    // So that we have acccess our controller
    QuestionController _questionController = Get.put(QuestionController());
    return Scaffold(
      body: questions.length > 0
          ? Stack(
              children: <Widget>[
                Image.asset("assets/images/cr-bg.png", fit: BoxFit.fill),
                SafeArea(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: kDefaultPadding),
                      ),
                      SizedBox(height: kDefaultPadding),
                      // Padding(
                      //   padding: const EdgeInsets.symmetric(
                      //       horizontal: kDefaultPadding),
                      //   child: Obx(
                      //     () => Text.rich(
                      //       TextSpan(
                      //         text: questions.length > 0
                      //             ? questions[currentquestions].question
                      //             : "",
                      //         style: Theme.of(context)
                      //             .textTheme
                      //             .headline4
                      //             .copyWith(color: Colors.black),
                      //         children: [
                      //           TextSpan(
                      //             text:
                      //                 "/${_questionController.questions.length}",
                      //             style: Theme.of(context)
                      //                 .textTheme
                      //                 .headline5
                      //                 .copyWith(color: Colors.black),
                      //           ),
                      //         ],
                      //       ),
                      //     ),
                      //   ),
                      // ),
                      Divider(thickness: 1.5),
                      SizedBox(height: kDefaultPadding),
                      Text(questions[currentquestions].question),

                      Expanded(
                          child: ListView.builder(
                        itemCount: questions[currentquestions].options.length,
                        itemBuilder: (context, index) {
                          return Container(
                              margin: EdgeInsets.symmetric(vertical: 10),
                              padding: EdgeInsets.all(10),
                              height: 70,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(13),
                                boxShadow: [
                                  BoxShadow(
                                    offset: Offset(0, 17),
                                    blurRadius: 23,
                                    spreadRadius: -13,
                                    color: kShadowColor,
                                  ),
                                ],
                              ),
                              child: ListTile(
                                title: Text(
                                    '${questions[currentquestions].options[index]}'),
                              ));
                        },
                      ))

                      // Expanded(
                      //   child: PageView.builder(
                      //     // Block swipe to next qn
                      //     physics: NeverScrollableScrollPhysics(),
                      //     controller: _questionController.pageController,
                      //     onPageChanged: _questionController.updateTheQnNum,
                      //     itemCount: questions.length,
                      //     itemBuilder: (context, index) => QuestionCard(
                      //         question: questions[currentquestions]),
                      //   ),
                      // ),
                    ],
                  ),
                )
              ],
            )
          : SizedBox(),
    );
  }
}

Future<List<Quizzes>> getquestions(quizId) async {
  print(quizId);
  final response = await http.get(
      Uri.https('cls.anak2u.com.my', 'api/quizzes/$quizId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      });

  if (response.statusCode == 200) {
    print("ok");
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    return Quizzes.questionsFromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

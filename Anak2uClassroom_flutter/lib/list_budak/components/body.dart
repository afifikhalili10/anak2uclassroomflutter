import 'package:flutter/material.dart';
// import 'package:flutter_auth/Screens/Login/components/background.dart';
import 'package:flutter_auth/list_budak/data.dart';
import 'package:flutter_auth/constants.dart';
import 'package:flutter_auth/list_budak/components/userlist.dart';

class Bodyselect extends StatelessWidget {
  const Bodyselect({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: background,
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Align(
              alignment: Alignment.topCenter,
              child: Text(
                '',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            SizedBox(height: size.height * 0.08),
            Image.asset(
              "assets/img/anak2U.png",
              width: 300,
              height: 150,
            ),
            Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              child: Stack(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(top: 10),
                    height: MediaQuery.of(context).size.height,
                    width: double.infinity,
                    child: ListView.builder(
                        itemCount: details.length,
                        itemBuilder: (BuildContext context, int index) {
                          return userList(context, index);
                        }),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

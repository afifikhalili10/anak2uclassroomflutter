import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_auth/Screens/mainpage/components/TopicDalamBahasaMelayu.dart';
import 'package:flutter_auth/Screens/mainpage/components/quiz.dart';
import 'package:flutter_auth/models/Activites.dart';
// import 'package:flutter_auth/models/lessons.dart';
// import 'package:flutter_auth/models/subject.dart';
// import 'package:flutter_svg/svg.dart';
import 'package:flutter_auth/constants.dart';
import 'package:flutter_auth/models/lessons.dart';
import 'package:flutter_auth/nota/nota_screen.dart';
import 'package:flutter_auth/quiz/components/body.dart';
import 'package:flutter_auth/video/video_screen.dart';
import 'package:flutter_auth/widgets/bottom_nav_bar.dart';
//import 'package:flutter_auth/widgets/search_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class BodyActivity extends StatefulWidget {
  final Lesson lesson;
  BodyActivity({this.lesson});

  @override
  _BodyActivityState createState() => _BodyActivityState();
}

class _BodyActivityState extends State<BodyActivity> {
  var activities = [];
  Widget activityimage(int activityType, double size) {
    if (activityType == 7) {
      return Image.asset('assets/img/latihantopik.png', width: size);
    }
    if (activityType == 2) {
      return Image.asset('assets/img/movie.png', width: size);
    }
    if (activityType == 4) {
      return Image.asset('assets/img/read.png', width: size);
    }
    return SizedBox();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getactivities(widget.lesson.id).then((value) {
      print(value);
      setState(() {
        activities = value;
      });
    }).catchError((error) => {print(error)});
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      bottomNavigationBar: BottomNavBar(),
      body: Stack(
        children: <Widget>[
          Container(
            //height: size.height * .45,
            decoration: BoxDecoration(
              color: kBlueLightColor,
              image: DecorationImage(
                image: AssetImage("assets/images/cr-bg.png"),
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
          SafeArea(
            child: Padding(
              child: Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  // Image.asset(
                  //   "assets/img/bahasamelayu.png",
                  //   width: 100,
                  // ),
                  Expanded(
                    child: ListView.separated(
                      padding: const EdgeInsets.all(8),
                      itemCount: activities.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                            margin: EdgeInsets.symmetric(vertical: 10),
                            padding: EdgeInsets.all(10),
                            height: 150,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(13),
                              boxShadow: [
                                BoxShadow(
                                  offset: Offset(0, 17),
                                  blurRadius: 23,
                                  spreadRadius: -13,
                                  color: kShadowColor,
                                ),
                              ],
                            ),
                            child: ListTile(
                              leading:
                                  activityimage(activities[index].type, 100),
                              subtitle: Row(children: [
                                activityimage(activities[index].type, 20),
                                SizedBox(
                                  height: 20,
                                ),
                                Text(activities[index].typename)
                              ]),
                              title: Text(
                                activities[index].title,
                                style: GoogleFonts.kronaOne(
                                    fontSize: 18, color: Colors.lightBlue),
                              ),
                              onTap: () {
                                if (activities[index].type == 7) {
                                  var quizid = jsonDecode(activities[index]
                                      .additionalfield)["quiz_id"];
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) {
                                      return BodyQuiz(quizid: quizid);
                                    }),
                                  );
                                } else if (activities[index].type == 2) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) {
                                      return BodyVideo();
                                    }),
                                  );
                                }
                                if (activities[index].type == 4) {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(builder: (context) {
                                      return BodyNota();
                                    }),
                                  );
                                }
                              },
                            ));
                      },
                      separatorBuilder: (BuildContext context, int index) =>
                          const Divider(),
                    ),
                  ),
                ],
              ),
              padding: const EdgeInsets.symmetric(horizontal: 20),
            ),
          ),
        ],
      ),
    );
  }
}

Future<List<Activity>> getactivities(lessonId) async {
  print(lessonId);
  final response = await http.get(
      Uri.https('cls.anak2u.com.my', 'api/classes/92/lessons/$lessonId'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      });

  if (response.statusCode == 200) {
    print("ok");
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    return Activity.activitiesFromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

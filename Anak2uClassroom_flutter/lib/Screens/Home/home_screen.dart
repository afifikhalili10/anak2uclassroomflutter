import 'package:flutter/material.dart';
import 'package:flutter_auth/Lesson/lesson_screen.dart';
import 'package:flutter_auth/Screens/mainpage/components/banksoalan.dart';
import 'package:flutter_auth/Screens/mainpage/components/BahasaMelayu.dart';
import 'package:flutter_auth/Screens/mainpage/components/quiz.dart';
import 'package:flutter_auth/Screens/mainpage/components/tanyajawab.dart';
//import 'package:flutter_auth/Screens/Mainpage/components/background.dart';
//import 'package:flutter_auth/Screens/mainpage/components/background.dart';
// ignore: unused_import
import 'package:flutter_auth/Screens/Login/components/body.dart';
// import 'package:flutter_auth/Screens/Signup/signup_screen.dart';
// import 'package:flutter_auth/Screens/mainpage/components/mainpage_screen.dart';
// import 'package:flutter_auth/components/already_have_an_account_acheck.dart';
// import 'package:flutter_auth/components/rounded_button.dart';
// import 'package:flutter_auth/components/rounded_input_field.dart';
// import 'package:flutter_auth/components/rounded_password_field.dart';
// ignore: unused_import
import 'dart:convert';
import 'package:flutter_auth/constants.dart';
import 'package:flutter_auth/models/subject.dart';
import 'package:flutter_auth/widgets/category_card_local.dart';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart' as http;
//import 'package:flutter_auth/widgets/search_bar.dart';
import 'package:flutter_auth/widgets/bottom_nav_bar.dart';
import 'package:flutter_auth/widgets/category_card.dart';

//import 'package:flutter/services.dart';
// components
//import 'package:flutter_auth/Screens/mainpage/components/mainpage_screen.dart';

class MainBody extends StatefulWidget {
  @override
  _MainBodyState createState() => _MainBodyState();
}

class _MainBodyState extends State<MainBody> {
  var subjects = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getSubjects().then((value) {
      print(value);
      setState(() {
        subjects = value;
      });
    }).catchError((error) => {print(error)});
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context)
        .size; //this gonna give us total height and with of our device
    return Scaffold(
      resizeToAvoidBottomInset: false,
      bottomNavigationBar: BottomNavBar(),
      body: Stack(
        children: <Widget>[
          Container(
            // Here the height of the container is 45% of our total height
            //height: size.height * .45,
            decoration: BoxDecoration(
              //color: Colors.transparent,
              image: DecorationImage(
                  alignment: Alignment.center,
                  image: AssetImage("assets/images/cr-bg.png"),
                  fit: BoxFit.cover),
            ),
          ),
          SafeArea(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Align(
                    alignment: Alignment.topLeft,
                    child: Container(
                      child: SvgPicture.asset("assets/icon/menu.svg"),
                      alignment: Alignment.center,
                      height: 142,
                      width: 52,
                      decoration: BoxDecoration(
                        color: Colors.black12,
                        shape: BoxShape.circle,
                      ),
                    ),
                  ),
                  // Text(
                  //   "Good Mornign \nShishir",
                  //   style: Theme.of(context)
                  //       .textTheme
                  //       .display1
                  //       .copyWith(fontWeight: FontWeight.w900),
                  // ),
                  Expanded(
                    child: GridView.builder(
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 3,
                          crossAxisSpacing: 10,
                          childAspectRatio: .75,
                          mainAxisSpacing: 10,
                        ),
                        itemCount: subjects.length,
                        itemBuilder: (BuildContext context, int index) {
                          return CategoryCard(
                            title: subjects[index].name,
                            svgSrc: subjects[index].iconUrl,
                            press: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(builder: (context) {
                                  return BodyLesson(subject: subjects[index]);
                                }),
                              );
                            },
                          );
                        }),
                  ),
                  Container(
                    constraints: BoxConstraints.expand(
                      height:
                          Theme.of(context).textTheme.headline4.fontSize * 1.1 +
                              50.0,
                    ),
                    padding: const EdgeInsets.all(8.0),
                    color: Colors.transparent,
                    alignment: Alignment.center,
                    child: Text('TAJUK',
                        style: Theme.of(context)
                            .textTheme
                            .headline4
                            .copyWith(color: Colors.black)),
                    transform: Matrix4.rotationZ(0.0),
                  ),

                  Expanded(
                    child: GridView.count(
                      crossAxisCount: 2,
                      childAspectRatio: .95,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 20,
                      children: <Widget>[
                        CategoryCardLocal(
                          title: "BANK SOALAN",
                          svgSrc: "assets/img/banksoalan.png",
                          press: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) {
                                return Quiz();
                              }),
                            );
                          },
                        ),
                        CategoryCardLocal(
                          title: "TANYA JAWAB  ",
                          svgSrc: "assets/img/tanyajawab.png",
                          press: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) {
                                return Latihan();
                              }),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<List<Subject>> getSubjects() async {
    final response = await http.get(
        Uri.https('cls.anak2u.com.my', 'api/students/subjects/92'),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        });

    if (response.statusCode == 200) {
      // If the server did return a 201 CREATED response,
      // then parse the JSON.
      return Subject.subjectsFromJson(jsonDecode(response.body));
    } else {
      // If the server did not return a 201 CREATED response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }
}

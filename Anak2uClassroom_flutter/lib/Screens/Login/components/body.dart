import 'package:flutter/material.dart';
import 'package:flutter_auth/Screens/Login/components/background.dart';
import 'package:flutter_auth/list_budak/components/body.dart';
// ignore: unused_import
import 'package:flutter_auth/models/login.dart';
import 'package:flutter_auth/Screens/Signup/signup_screen.dart';
import 'package:flutter_auth/Screens/home/home_screen.dart';
import 'package:flutter_auth/components/already_have_an_account_acheck.dart';
import 'package:flutter_auth/components/rounded_button.dart';
import 'package:flutter_auth/components/rounded_input_field.dart';
import 'package:flutter_auth/components/rounded_password_field.dart';
// ignore: unused_import
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //  Text(
            //     "LOGIN",
            //     style: TextStyle(
            //        fontFamily: 'Arial',
            //         fontSize: 20,
            //         fontWeight: FontWeight.bold),
            //   ),

            SizedBox(height: size.height * 0.34),
            TextField(
              controller: emailController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(), hintText: 'Email'),
              // hintText: "Your Email",
            ),
            TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(), hintText: 'Password'),
              controller: passwordController,
            ),
            RoundedButton(
              text: "LOGIN",
              color: Colors.lightBlueAccent,
              textColor: Colors.black,
              press: () {
                login(emailController.text, passwordController.text)
                    .then((value) {
                  print(value.token);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return MainBody();
                      },
                    ),
                  );
                }).catchError((error) => {print(error)});
              },
            ),
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SignUpScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

Future<LoginModel> login(String email, password) async {
  final response = await http.post(
    Uri.https('dashboard.anak2u.com.my', 'api/v2/parentlogin'),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(<String, String>{'email': email, 'password': password}),
  );
  if (response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    return LoginModel.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

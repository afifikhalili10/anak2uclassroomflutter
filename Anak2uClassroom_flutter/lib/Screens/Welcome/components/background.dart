import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  final Widget child;
  const Background({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      height: size.height,
      width: double.infinity,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            top: -210,
            //left: 0,

            child: Image.asset(
              "assets/images/anak2u-hd.png",
              width: size.width * 0.40,
              height: size.height * 0.8,
            ),
          ),
          child,
        ],
      ),
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/images/classroomLogin.png'),
            fit: BoxFit.cover),
      ),
    );
  }
}

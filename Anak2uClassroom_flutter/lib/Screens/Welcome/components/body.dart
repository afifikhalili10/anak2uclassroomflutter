import 'package:flutter/material.dart';
import 'package:flutter_auth/Screens/Login/login_screen.dart';
import 'package:flutter_auth/Screens/Signup/signup_screen.dart';
import 'package:flutter_auth/Screens/Welcome/components/background.dart';
import 'package:flutter_auth/components/rounded_button.dart';
// ignore: unused_import
import 'package:flutter_auth/constants.dart';
// ignore: unused_import
import 'package:flutter_svg/svg.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // This size provide us total height and width of our screen
    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //Text(
            //"WELCOME TO ANAK2U ",
            //  style: TextStyle(
            //   fontFamily: 'Arial',
            //   fontSize: 25,
            //   fontWeight: FontWeight.bold),
            //),

            SizedBox(height: size.height * 0.40),
            //SvgPicture.asset(
            //"assets/icons/anak2u-logo1.svg",
            //height: size.height * 0.45,
            // ),
            SizedBox(height: size.height * 0.25),
            RoundedButton(
              text: "LOGIN",
              textColor: Colors.black,
              color: Colors.lightBlueAccent,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              },
            ),
            RoundedButton(
              text: "REGISTER",
              color: Colors.lightBlue, //kPrimaryLightColor,
              textColor: Colors.black,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SignUpScreen();
                    },
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_auth/Screens/mainpage/components/banksoalan.dart';
import 'package:flutter_auth/Screens/mainpage/components/BahasaMelayu.dart';
import 'package:flutter_auth/Screens/mainpage/components/quiz.dart';
import 'package:flutter_auth/Screens/mainpage/components/tanyajawab.dart';
//import 'package:flutter_auth/Screens/Mainpage/components/background.dart';
//import 'package:flutter_auth/Screens/mainpage/components/background.dart';
// ignore: unused_import

// import 'package:flutter_auth/Screens/Signup/signup_screen.dart';
// import 'package:flutter_auth/Screens/mainpage/components/mainpage_screen.dart';
// import 'package:flutter_auth/components/already_have_an_account_acheck.dart';
// import 'package:flutter_auth/components/rounded_button.dart';
// import 'package:flutter_auth/components/rounded_input_field.dart';
// import 'package:flutter_auth/components/rounded_password_field.dart';
// ignore: unused_import
import 'package:flutter_auth/constants.dart';

//import 'package:flutter_auth/widgets/search_bar.dart';

//import 'package:flutter/services.dart';
// components
import 'package:flutter_auth/Screens/Home/home_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: ' App',
      theme: ThemeData(
        fontFamily: "Cairo",
        scaffoldBackgroundColor: kBackgroundColor,
        textTheme: Theme.of(context).textTheme.apply(displayColor: kTextColor),
      ),
      home: MainBody(),
    );
  }
}

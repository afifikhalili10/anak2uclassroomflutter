// import 'package:flutter/material.dart';
// import 'package:flutter_auth/Screens/mainpage/components/quizlatihanBahasaMelayu.dart';
// //import 'package:flutter_svg/svg.dart';
// import 'package:flutter_auth/constants.dart';
// import 'package:flutter_auth/quiz/components/Body.dart';
// import 'package:flutter_auth/widgets/bottom_nav_bar.dart';
// //import 'package:flutter_auth/widgets/search_bar.dart';
// import 'package:google_fonts/google_fonts.dart';

// // ignore: must_be_immutable
// class SelectId extends StatelessWidget {
//   var contents = [
//     {
//       "title": "KELUARGA SAYA",
//       "url": "assets/img/checklist.png",
//       "types": "KUIZ",
//       "typeimage": "assets/img/activity.png"
//     },
//     {
//       "title": "MENGENAL UNIT MASA",
//       "url": "assets/img/movie.png",
//       "types": "VIDEO",
//       "typeimage": "assets/img/activity.png"
//     },
//     {
//       "title": "KELUARGA SAYA",
//       "url": "assets/img/checklist.png",
//       "types": "KUIZ",
//       "typeimage": "assets/img/activity.png"
//     },
//     {
//       "title": "TEST",
//       "url": "assets/img/baca.png",
//       "types": "KUIZ",
//       "typeimage": "assets/img/activity.png"
//     },
//     {
//       "title": "TEST",
//       "url": "assets/img/baca.png",
//       "types": "KUIZ",
//       "typeimage": "assets/img/activity.png"
//     },
//     {
//       "title": "TEST",
//       "url": "assets/img/baca.png",
//       "types": "KUIZ",
//       "typeimage": "assets/img/activity.png"
//     },
//     {
//       "title": "KELUARGA SAYA",
//       "url": "assets/img/checklist.png",
//       "types": "KUIZ",
//       "typeimage": "assets/img/activity.png"
//     },
//     {
//       "title": "KELUARGA SAYA",
//       "url": "assets/img/checklist.png",
//       "types": "KUIZ",
//       "typeimage": "assets/img/activity.png"
//     },
//     {
//       "title": "TEST",
//       "url": "assets/img/movie.png",
//       "types": "KUIZ",
//       "typeimage": "assets/img/activity.png"
//     },
//     {
//       "title": "KELUARGA SAYA",
//       "url": "assets/img/checklist.png",
//       "types": "KUIZ",
//       "typeimage": "assets/img/activity.png"
//     },
//     {
//       "title": "YOUTUBE TEST",
//       "url": "assets/img/movie.png",
//       "types": "KUIZ",
//       "typeimage": "assets/img/activity.png"
//     },
//     {
//       "title": "EXOPLANETS:THE HUNT FOR HABITABLE WORLDS",
//       "url": "assets/img/movie.png",
//       "types": "KUIZ",
//       "typeimage": "assets/img/activity.png"
//     }
//   ];
//   @override
//   Widget build(BuildContext context) {
//     var size = MediaQuery.of(context).size;
//     return Scaffold(
//       bottomNavigationBar: BottomNavBar(),
//       body: Stack(
//         children: <Widget>[
//           Container(
//             //height: size.height * .45,
//             decoration: BoxDecoration(
//               color: kBlueLightColor,
//               image: DecorationImage(
//                 image: AssetImage("assets/images/cr-bg.png"),
//                 fit: BoxFit.fitWidth,
//               ),
//             ),
//           ),
//           SafeArea(
//             child: Padding(
//               child: Column(
//                 children: [
//                   SizedBox(
//                     height: 10,
//                   ),
//                   // Image.asset(
//                   //   "assets/img/bahasamelayu.png",
//                   //   width: 100,
//                   // ),
//                   Expanded(
//                     child: ListView.separated(
//                       padding: const EdgeInsets.all(8),
//                       itemCount: contents.length,
//                       itemBuilder: (BuildContext context, int index) {
//                         return Container(
//                             margin: EdgeInsets.symmetric(vertical: 10),
//                             padding: EdgeInsets.all(10),
//                             height: 150,
//                             decoration: BoxDecoration(
//                               color: Colors.white,
//                               borderRadius: BorderRadius.circular(13),
//                               boxShadow: [
//                                 BoxShadow(
//                                   offset: Offset(0, 17),
//                                   blurRadius: 23,
//                                   spreadRadius: -13,
//                                   color: kShadowColor,
//                                 ),
//                               ],
//                             ),
//                             child: ListTile(
//                               leading: Image.asset(contents[index]["url"]),
//                               subtitle: Row(children: [
//                                 Image.asset(
//                                   contents[index]["typeimage"],
//                                   width: 40,
//                                 ),
//                                 SizedBox(
//                                   height: 20,
//                                 ),
//                                 Text(contents[index]["types"])
//                               ]),
//                               title: Text(
//                                 contents[index]["title"],
//                                 style: GoogleFonts.kronaOne(
//                                     fontSize: 18, color: Colors.lightBlue),
//                               ),
//                               onTap: () {
//                                 Navigator.push(
//                                   context,
//                                   MaterialPageRoute(builder: (context) {
//                                     return Body();
//                                   }),
//                                 );
//                               },
//                             ));
//                       },
//                       separatorBuilder: (BuildContext context, int index) =>
//                           const Divider(),
//                     ),
//                   ),
//                 ],
//               ),
//               padding: const EdgeInsets.symmetric(horizontal: 20),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }

// class SeassionCard extends StatelessWidget {
//   final int seassionNum;
//   final bool isDone;
//   final Function press;
//   const SeassionCard({
//     Key key,
//     this.seassionNum,
//     this.isDone = false,
//     this.press,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return LayoutBuilder(builder: (context, constraint) {
//       return ClipRRect(
//         borderRadius: BorderRadius.circular(13),
//         child: Container(
//           width: constraint.maxWidth / 2 -
//               10, // constraint.maxWidth provide us the available with for this widget
//           // padding: EdgeInsets.all(16),
//           decoration: BoxDecoration(
//             color: Colors.white,
//             borderRadius: BorderRadius.circular(13),
//             boxShadow: [
//               BoxShadow(
//                 offset: Offset(0, 17),
//                 blurRadius: 23,
//                 spreadRadius: -13,
//                 color: kShadowColor,
//               ),
//             ],
//           ),
//           child: Material(
//             color: Colors.transparent,
//             child: InkWell(
//               onTap: press,
//               child: Padding(
//                 padding: const EdgeInsets.all(16.0),
//                 child: Row(
//                   children: <Widget>[
//                     Container(
//                       height: 42,
//                       width: 43,
//                       decoration: BoxDecoration(
//                         color: isDone ? kBlueColor : Colors.white,
//                         shape: BoxShape.circle,
//                         border: Border.all(color: kBlueColor),
//                       ),
//                       child: Icon(
//                         Icons.play_arrow,
//                         color: isDone ? Colors.white : kBlueColor,
//                       ),
//                     ),
//                     SizedBox(width: 10),
//                     Text(
//                       "Session $seassionNum",
//                       style: Theme.of(context).textTheme.subtitle,
//                     )
//                   ],
//                 ),
//               ),
//             ),
//           ),
//         ),
//       );
//     });
//   }
// }

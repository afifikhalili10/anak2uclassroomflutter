import 'package:flutter/material.dart';

class Background extends StatelessWidget {
  final Widget child;
  const Background({
    Key key,
    @required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: double.infinity,
      height: size.height,
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Positioned(
            top: -20,
            // left: 0,
            // child: Image.asset(
            //   "assets/images/main_top.png",
            //    width: size.width * 0.35,
            //   ),
            //  ),
            //
            child: Image.asset(
              "assets/images/drawable-xxxhdpi-icon.png",
              width: size.width * 0.40,
              height: size.height * 0.8,
            ),
            //  Positioned(
            //    bottom: 0,
            //    right: 0,
            //   child: Image.asset(
            //  "assets/images/login_bottom.png",
            //    width: size.width * 0.4,
            //    ),
          ),
          child,
        ],
      ),
      decoration: BoxDecoration(
        image: DecorationImage(
            image: AssetImage('assets/images/cr-bg.png'), fit: BoxFit.cover),
      ),
    );
  }
}

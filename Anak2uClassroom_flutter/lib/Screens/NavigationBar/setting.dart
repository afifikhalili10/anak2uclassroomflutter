import 'package:flutter_auth/Screens/NavigationBar/profile.dart';
import 'package:settings_ui/settings_ui.dart';

import 'package:flutter/material.dart';
//import 'package:flutter_auth/Screens/mainpage/components/TopicDalamBahasaMelayu.dart';
//import 'package:flutter_svg/svg.dart';
import 'package:flutter_auth/constants.dart';
import 'package:flutter_auth/widgets/bottom_nav_bar.dart';
//import 'package:flutter_auth/widgets/search_bar.dart';

// ignore: must_be_immutable
class Settings extends StatelessWidget {
  var topics = [
    {
      "title": "KELUARGA SAYA",
    },
    {"title": "DSF"},
    {"title": "LATIHAN SAYA"}
  ];

  get value => null;
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      bottomNavigationBar: BottomNavBar(),
      body: Stack(
        children: <Widget>[
          Container(
            //height: size.height * .45,
            decoration: BoxDecoration(
              color: kBlueLightColor,
              image: DecorationImage(
                image: AssetImage("assets/images/cr-bg.png"),
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
          SettingsList(
            sections: [
              SettingsSection(
                title: 'Section',
                tiles: [
                  SettingsTile(
                    title: 'Profile',
                    leading: Icon(Icons.supervised_user_circle),
                    // ignore: deprecated_member_use
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return Profile();
                        }),
                      );
                    },
                  ),
                  SettingsTile(
                    title: 'Language',
                    subtitle: 'English',
                    leading: Icon(Icons.language),
                    onPressed: (BuildContext context) {},
                  ),
                ],
              ),
              SettingsSection(
                title: 'Security',
                tiles: [
                  SettingsTile(
                    title: 'Authentication',
                    leading: Icon(Icons.supervised_user_circle),
                    onPressed: (BuildContext context) {},
                  ),
                  SettingsTile(
                    title: 'Change Password',
                    leading: Icon(Icons.language),
                    onPressed: (BuildContext context) {},
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}

class SeassionCard extends StatelessWidget {
  final int seassionNum;
  final bool isDone;
  final Function press;
  const SeassionCard({
    Key key,
    this.seassionNum,
    this.isDone = false,
    this.press,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraint) {
      return ClipRRect(
        borderRadius: BorderRadius.circular(13),
        child: Container(
          width: constraint.maxWidth / 2 -
              10, // constraint.maxWidth provide us the available with for this widget
          // padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(13),
            boxShadow: [
              BoxShadow(
                offset: Offset(0, 17),
                blurRadius: 23,
                spreadRadius: -13,
                color: kShadowColor,
              ),
            ],
          ),
          child: Material(
            color: Colors.black,
            child: InkWell(
              onTap: press,
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      height: 42,
                      width: 43,
                      decoration: BoxDecoration(
                        color: isDone ? kBlueColor : Colors.white,
                        shape: BoxShape.circle,
                        border: Border.all(color: kBlueColor),
                      ),
                      child: Icon(
                        Icons.play_arrow,
                        color: isDone ? Colors.white : kBlueColor,
                      ),
                    ),
                    SizedBox(width: 10),
                    Text(
                      "Session $seassionNum",
                      style: Theme.of(context).textTheme.subtitle,
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      );
    });
  }
}

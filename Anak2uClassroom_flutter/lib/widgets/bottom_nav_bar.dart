import 'package:flutter/material.dart';
import 'package:flutter_auth/Screens/NavigationBar/progress.dart';
import 'package:flutter_auth/Screens/NavigationBar/reference.dart';
import 'package:flutter_auth/Screens/NavigationBar/setting.dart';
import 'package:flutter_auth/Screens/mainpage/components/body.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_auth/constants.dart';
import 'package:flutter_auth/Screens/home/home_screen.dart';

class BottomNavBar extends StatelessWidget {
  const BottomNavBar({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
      height: 80,
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          BottomNavItem(
            title: "Home",
            svgScr: "assets/icon/home.svg",
            press: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
                  return MainBody();
                }),
              );
            },
          ),
          BottomNavItem(
            title: "Progress",
            svgScr: "assets/icon/chart.svg",
            press: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
                  return DemoApp();
                }),
              );
            },
            //isActive: true,
          ),
          BottomNavItem(
            title: "Reference",
            svgScr: "assets/icon/reference.svg",
            press: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
                  return Reference();
                }),
              );
            },
          ),
          BottomNavItem(
            title: "Settings",
            svgScr: "assets/icon/Settings.svg",
            press: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) {
                  return Settings();
                }),
              );
            },
          ),
        ],
      ),
    );
  }
}

class BottomNavItem extends StatelessWidget {
  final String svgScr;
  final String title;
  final Function press;
  final bool isActive;
  const BottomNavItem({
    Key key,
    this.svgScr,
    this.title,
    this.press,
    this.isActive = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: press,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          SvgPicture.asset(
            svgScr,
            color: isActive ? kActiveIconColor : kTextColor,
          ),
          Text(
            title,
            style: TextStyle(color: isActive ? kActiveIconColor : kTextColor),
          ),
        ],
      ),
    );
  }
}

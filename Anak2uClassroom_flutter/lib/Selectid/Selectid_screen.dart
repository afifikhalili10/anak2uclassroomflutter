// import 'package:flutter/material.dart';
// import 'package:flutter_auth/Screens/Home/home_screen.dart';
// import 'package:flutter_auth/Screens/mainpage/components/TopicDalamBahasaMelayu.dart';
// //import 'package:flutter_auth/models/lessons.dart';
// import 'package:flutter_auth/models/selectid.dart';
// //import 'package:flutter_svg/svg.dart';
// import 'package:flutter_auth/constants.dart';
// import 'package:flutter_auth/widgets/bottom_nav_bar.dart';
// //import 'package:flutter_auth/widgets/search_bar.dart';
// import 'package:google_fonts/google_fonts.dart';
// import 'package:http/http.dart' as http;
// import 'dart:convert';

// class BodyStudent extends StatefulWidget {
//   final Student data;
//   BodyStudent({this.data});
//   @override
//   _BodyStudentState createState() => _BodyStudentState();
// }

// class _BodyStudentState extends State<BodyStudent> {
//   var selectid = [];

//   @override
//   void initState() {
//     // TODO: implement initState
//     super.initState();
//     getstudents(widget.data.id).then((value) {
//       print(value);
//       setState(() {
//         selectid = value;
//       });
//     }).catchError((error) => {print(error)});
//   }

//   @override
//   Widget build(BuildContext context) {
//     var size = MediaQuery.of(context).size;
//     return Scaffold(
//       bottomNavigationBar: BottomNavBar(),
//       body: Stack(
//         children: <Widget>[
//           Container(
//             //height: size.height * .45,
//             decoration: BoxDecoration(
//               color: kBlueLightColor,
//               image: DecorationImage(
//                 image: AssetImage("assets/images/cr-bg.png"),
//                 fit: BoxFit.fitWidth,
//               ),
//             ),
//           ),
//           SafeArea(
//             child: Padding(
//               child: Column(
//                 children: [
//                   SizedBox(
//                     height: 10,
//                   ),
//                   // Image.network(
//                   //   widget.subject.iconUrl,
//                   //   width: 100,
//                   // ),
//                   Expanded(
//                     child: ListView.separated(
//                       padding: const EdgeInsets.all(8),
//                       itemCount: selectid.length,
//                       itemBuilder: (BuildContext context, int index) {
//                         return Container(
//                             margin: EdgeInsets.symmetric(vertical: 10),
//                             padding: EdgeInsets.all(10),
//                             height: 70,
//                             decoration: BoxDecoration(
//                               color: Colors.white,
//                               borderRadius: BorderRadius.circular(13),
//                               boxShadow: [
//                                 BoxShadow(
//                                   offset: Offset(0, 17),
//                                   blurRadius: 23,
//                                   spreadRadius: -13,
//                                   color: kShadowColor,
//                                 ),
//                               ],
//                             ),
//                             child: ListTile(
//                               title: Text(
//                                 selectid[index].title,
//                                 style: GoogleFonts.kronaOne(
//                                     fontSize: 20, color: Colors.lightBlue),
//                               ),
//                               onTap: () {
//                                 Navigator.push(
//                                   context,
//                                   MaterialPageRoute(builder: (context) {
//                                     return LatihanDetail();
//                                   }),
//                                 );
//                               },
//                             ));
//                       },
//                       separatorBuilder: (BuildContext context, int index) =>
//                           const Divider(),
//                     ),
//                   ),
//                 ],
//               ),
//               padding: const EdgeInsets.symmetric(horizontal: 20),
//             ),
//           ),
//         ],
//       ),
//     );
//   }
// }

// Future<List<Student>> getstudents(studentid) async {
//   print(studentid);
//   final response = await http.get(
//       Uri.https('dashboard.anak2u.com.my', 'api/v2/students/$studentid'),
//       headers: <String, String>{
//         'Content-Type': 'application/json; charset=UTF-8',
//       });

//   if (response.statusCode == 200) {
//     print("ok");
//     // If the server did return a 201 CREATED response,
//     // then parse the JSON.
//     return Student.studentsFromJson(jsonDecode(response.body));
//   } else {
//     // If the server did not return a 201 CREATED response,
//     // then throw an exception.
//     throw Exception('Failed to load album');
//   }
// }

import 'package:flutter/material.dart';
import 'package:flutter_auth/Screens/mainpage/components/TopicDalamBahasaMelayu.dart';
import 'package:flutter_auth/activity/activities_screen.dart';
import 'package:flutter_auth/models/lessons.dart';
import 'package:flutter_auth/models/subject.dart';
import 'package:flutter_svg/svg.dart';
import 'package:flutter_auth/constants.dart';
import 'package:flutter_auth/widgets/bottom_nav_bar.dart';
//import 'package:flutter_auth/widgets/search_bar.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class BodyNota extends StatefulWidget {
  // final Subject subject;
  // BodyLesson({this.subject});
  @override
  _BodyNotaState createState() => _BodyNotaState();
}

class _BodyNotaState extends State<BodyNota> {
  var lessons = [];

  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();
  //   getlessons(widget.subject.id).then((value) {
  //     print(value);
  //     setState(() {
  //       lessons = value;
  //     });
  //   }).catchError((error) => {print(error)});
  // }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      bottomNavigationBar: BottomNavBar(),
      body: Stack(
        children: <Widget>[
          Container(
            //height: size.height * .45,
            decoration: BoxDecoration(
              color: kBlueLightColor,
              image: DecorationImage(
                image: AssetImage("assets/images/cr-bg.png"),
                fit: BoxFit.fitWidth,
              ),
            ),
          ),
          SafeArea(
            child: Padding(
              child: Column(
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  // Image.network(
                  //   widget.subject.iconUrl,
                  //   width: 100,
                  // ),
                  Expanded(
                    child: ListView.separated(
                      padding: const EdgeInsets.all(8),
                      itemCount: lessons.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                            margin: EdgeInsets.symmetric(vertical: 10),
                            padding: EdgeInsets.all(10),
                            height: 70,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(13),
                              boxShadow: [
                                BoxShadow(
                                  offset: Offset(0, 17),
                                  blurRadius: 23,
                                  spreadRadius: -13,
                                  color: kShadowColor,
                                ),
                              ],
                            ),
                            child: ListTile(
                              title: Text(
                                lessons[index].title,
                                style: GoogleFonts.kronaOne(
                                    fontSize: 20, color: Colors.lightBlue),
                              ),
                              onTap: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) {
                                    return BodyActivity(lesson: lessons[index]);
                                  }),
                                );
                              },
                            ));
                      },
                      separatorBuilder: (BuildContext context, index) =>
                          const Divider(),
                    ),
                  ),
                ],
              ),
              padding: const EdgeInsets.symmetric(horizontal: 20),
            ),
          ),
        ],
      ),
    );
  }
}

// Future<List<Lesson>> getlessons(subjectId) async {
//   print(subjectId);
//   final response = await http.get(
//       Uri.https(
//           'cls.anak2u.com.my', 'api/students/class/92/subject/$subjectId'),
//       headers: <String, String>{
//         'Content-Type': 'application/json; charset=UTF-8',
//       });

//   if (response.statusCode == 200) {
//     print("ok");
//     // If the server did return a 201 CREATED response,
//     // then parse the JSON.
//     return Lesson.lessonsFromJson(jsonDecode(response.body));
//   } else {
//     // If the server did not return a 201 CREATED response,
//     // then throw an exception.
//     throw Exception('Failed to load album');
//   }
// }
